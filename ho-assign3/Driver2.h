//
//  Driver2.h
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called Book that contains instance data for the title,
//	author, publisher, and copyright date. Define the Book constructor to accept and initialize
//	this data. Include setter and getter methods for all instance data. Include a description
//	method that returns a nicely formatted (NSString *), multi-line description of the book.
//	Create a class Driver2  whose "run" method instantiates, updates, and displays several Book objects.
//
// Inputs:   none
// Outputs:  description of the book
//
// ******************************************************************************

#import <Foundation/Foundation.h>

@interface Driver2 : NSObject

- (void)run;

@end
