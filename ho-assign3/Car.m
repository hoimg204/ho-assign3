//
//  Car.m
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Car that contains instance data that represents the make,
//	model, and year of the car. Define the Car constructor to initialize these values.
//	Include getter and setter methods for all instance data, and a description method
//	that returns a one-line (NSString *)  description of the car.
//	Create a class Driver1  whose "run" method instantiates, updates, and displays several Car objects.
//
// Inputs:   none
// Outputs:  description of the car
//
// ******************************************************************************

#import "Car.h"

@implementation Car
{
	// instance variables
	NSString * make;
	NSString * model;
	int year;
}

// custom constructor
- (id) initWithMake:(NSString *)carMake andModel:(NSString *)carModel andYear:(int)carYear
{
	self = [super init];
	if (self)
	{
		// initialization happens here ...
		make = carMake;
		model = carModel;
		year = carYear;
	}
	return self;
}

// Make: setter and getter
- (void) setMake: (NSString *) carMake
{
	make = carMake;
}

- (NSString *) make
{
	return make;
}

//Model: setter and getter
- (void) setModel: (NSString *) carModel
{
	model = carModel;
}

- (NSString *) model
{
	return model;
}

//Year: setter and getter
- (void) setYear: (int) carYear
{
	year = carYear;
}

- (int) year
{
	return year;
}

// method "description" to display values of instance variables
- (NSString *) description
{
	NSString *str = @"";
	str = [str stringByAppendingString: @"Car Make is "];
	str = [str stringByAppendingString: make];
	str = [str stringByAppendingString: @", Model is " ];
	str = [str stringByAppendingString: model];
	str = [str stringByAppendingString:@",and Year "];
	str = [str stringByAppendingFormat:@"%i\n\n", year];
	return str;
}

@end
