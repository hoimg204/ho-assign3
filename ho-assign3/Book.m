//
//  Book.m
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called Book that contains instance data for the title,
//	author, publisher, and copyright date. Define the Book constructor to accept and initialize
//	this data. Include setter and getter methods for all instance data. Include a description
//	method that returns a nicely formatted (NSString *), multi-line description of the book.
//	Create a class Driver2  whose "run" method instantiates, updates, and displays several Book objects.
//
// Inputs:   none
// Outputs:  description of the book
//
// ******************************************************************************

#import "Book.h"

@implementation Book
{
	NSString *title;
	NSString *author;
	NSString *publisher;
	NSString *date;
}

- (id)initWithTitle:(NSString *)bookTitle andAuthor:(NSString *)bookAuthor andPublisher:(NSString *)bookPublisher andDate:(NSString *)bookDate
{
	self = [super init];
	if (self) {
		// initialization happens here ...
		title = bookTitle;
		author = bookAuthor;
		publisher = bookPublisher;
		date = bookDate;
	}
	return self;
}

// Title: setter and getter
- (void)setTitle:(NSString *)bookTitle
{
	title = bookTitle;
}

- (NSString *)title
{
	return title;
}

// Author: setter and getter
- (void)setAuthor:(NSString *)bookAuthor
{
	author = bookAuthor;
}

- (NSString *)author
{
	return author;
}

// Publisher: setter and getter
- (void)setPublisher:(NSString *)bookPublisher
{
	publisher = bookPublisher;
}

- (NSString *)publisher
{
	return publisher;
}

// Date: setter and getter
- (void)setDate:(NSString *)bookDate
{
	date = bookDate;
}

- (NSString *)date
{
	return date;
}

// method "description" to display values of instance variables
- (NSString *)description
{
	NSString *str = @"";
	str = [str stringByAppendingString:@"\ntitle: "];
	str = [str stringByAppendingString:title];
	str = [str stringByAppendingString:@"\nauthor: "];
	str = [str stringByAppendingString:author];
	str = [str stringByAppendingString:@"\npublisher: "];
	str = [str stringByAppendingString:publisher];
	str = [str stringByAppendingString:@"\ndate: "];
	str = [str stringByAppendingString:date];
	str = [str stringByAppendingString:@"\n\n"];
	return str;
}

@end
