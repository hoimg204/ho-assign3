//
//  Driver1.h
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Car that contains instance data that represents the make,
//	model, and year of the car. Define the Car constructor to initialize these values.
//	Include getter and setter methods for all instance data, and a description method
//	that returns a one-line (NSString *)  description of the car.
//	Create a class Driver1  whose "run" method instantiates, updates, and displays several Car objects.
//
// Inputs:   none
// Outputs:  description of the car
//
// ******************************************************************************

#import <Foundation/Foundation.h>

@interface Driver1 : NSObject

- (void) run;

@end
