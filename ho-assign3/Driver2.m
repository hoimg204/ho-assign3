//
//  Driver2.m
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called Book that contains instance data for the title,
//	author, publisher, and copyright date. Define the Book constructor to accept and initialize
//	this data. Include setter and getter methods for all instance data. Include a description
//	method that returns a nicely formatted (NSString *), multi-line description of the book.
//	Create a class Driver2  whose "run" method instantiates, updates, and displays several Book objects.
//
// Inputs:   none
// Outputs:  description of the book
//
// ******************************************************************************

#import "Driver2.h"
#import "Book.h"

@implementation Driver2 : NSObject

- (void)run
{
    //Object book1
    Book *book1 = [[Book alloc] init];
    [book1 setTitle:@"\"Objective-C\""];
    [book1 setAuthor:@"\"Someone 1\""];
    [book1 setPublisher:@"\"Company 1\""];
    [book1 setDate:@"\"2013-08-22\""];
    
    //Object book2
    Book *book2 = [[Book alloc] init];
    [book2 setTitle:@"\"Java\""];
    [book2 setAuthor:@"\"Someone 2\""];
    [book2 setPublisher:@"\"Company 2\""];
    [book2 setDate:@"\"2014-09-23\""];
    
    // use the Book's "description" method
    NSLog(@"%@", book1);
    NSLog(@"%@", book2);
}

@end
