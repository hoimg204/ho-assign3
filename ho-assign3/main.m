//
//  main.h
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Car that contains instance data that represents the make,
//	model, and year of the car. Define the Car constructor to initialize these values.
//	Include getter and setter methods for all instance data, and a description method
//	that returns a one-line (NSString *)  description of the car.
//	Create a class Driver1  whose "run" method instantiates, updates, and displays several Car objects.
//
// Inputs:   none
// Outputs:  description of the car
//
// ******************************************************************************
//
// Problem Statement 2:
//  Design and implement a class called Book that contains instance data for the title,
//	author, publisher, and copyright date. Define the Book constructor to accept and initialize
//	this data. Include setter and getter methods for all instance data. Include a description
//	method that returns a nicely formatted (NSString *), multi-line description of the book.
//	Create a class Driver2  whose "run" method instantiates, updates, and displays several Book objects.
//
// Inputs:   none
// Outputs:  description of the book
//
// ******************************************************************************

#import <Foundation/Foundation.h>
#import "Driver1.h"
#import "Driver2.h"

int main(int argc, const char *argv[])
{
	@autoreleasepool{
		//Object car
		Driver1 *car = [[Driver1 alloc] init];
		[car run];
		
		//Obeject book
		Driver2 *book = [[Driver2 alloc] init];
		[book run];
	}
	return 0;
}
