//
//  Driver1.m
//  ho-assign3
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-23.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Car that contains instance data that represents the make,
//	model, and year of the car. Define the Car constructor to initialize these values.
//	Include getter and setter methods for all instance data, and a description method
//	that returns a one-line (NSString *)  description of the car.
//	Create a class Driver1  whose "run" method instantiates, updates, and displays several Car objects.
//
// Inputs:   none
// Outputs:  description of the car
//
// ******************************************************************************

#import "Driver1.h"
#import "Car.h"

@implementation Driver1

- (void)run
{
	//Object car1
	Car *car1 = [[Car alloc] init];
	[car1 setMake:@"Ford"];
	[car1 setModel:@"New Beatle"];
	[car1 setYear:2010];
	
	//Object car2
	Car *car2 = [[Car alloc] init];
	[car2 setMake:@"Honda"];
	[car2 setModel:@"Civic"];
	[car2 setYear:2014];
	
	// use the Car's "description" method
	NSLog(@"%@", car1);
	NSLog(@"%@", car2);
}

@end
